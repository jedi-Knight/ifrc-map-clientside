config = {
    "map-of": "NDRRC - Flagship 4 Projects",
    "map-options": {
        "init-center": [28.478348, 86.542285],
        "map-bounds": { //78.739439,26.487043,89.847341,30.688485
            "northeast": [30.688485, 89.847341],
            "southwest": [26.487043, 78.739439],
        },
        "init-zoom": 7,
        "min-zoom": 7,
        "max-zoom": 11
    },
    "api": {
        "url": "/api/",
        "requestType": "GET"
    },
    "map-features": {
        "operational": {
            "src": "projects/geojson/?format=json",
            "title": "Flagship-4 Projects",
            "icon-src": "markers/operational.png"
        },/*
        "construction-approved": {
            "src": "construction-approved.geojson",
            "title": "Construction License Approved",
            "icon-src": "markers/construction-approved.png"
        },
        "construction-applied": {
            "src": "construction-applied.geojson",
            "title": "Applied for Construction License",
            "icon-src": "markers/construction-applied.png"
        },
        "survey-approved": {
            "src": "survey-approved.geojson",
            "title": "Survey License Approved",
            "icon-src": "markers/survey-approved.png"
        },
        "survey-applied": {
            "src": "survey-applied.geojson",
            "title": "Applied for Survey License",
            "icon-src": "markers/survey-applied.png"
        },

        "other-projects": {
            "src": "other-projects.geojson",
            "title": "Other Projects",
            "icon-src": "markers/other-projects.png"
        },*/

        "all-projects": {
            "src": "all-projects.geojson",
            "title": "All Projects",
            "icon-src": "markers/all-projects.png"
        }
    },
    "layer-styles": {
        "extent-marquee": {
            color: "#666666",
            opacity: 0.8,
            weight: 1,
            fillColor: "#aaccee",
            fillOpacity: 0.4,
            clickable: false
        },
        "highlight-circle": {
            fillOpacity: 0,
            opacity: 0.8,
            weight: 2,
            color: "#0080ff",
            radius: 20
        },
        "markers": {
            "operational": {
                "small": {
                    color: "#ffffff",
                    fillColor: "#0080ff",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 5
                },
                "medium": {
                    color: "#ffffff",
                    fillColor: "#0080ff",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 10
                },
                "large": {
                    color: "#ffffff",
                    fillColor: "#0080ff",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 15
                },
                "mega": {
                    color: "#ffffff",
                    fillColor: "#0080ff",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 20
                }
            },
            "construction-approved": {
                "small": {
                    color: "#ffffff",
                    fillColor: "#a6d854",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 5
                },
                "medium": {
                    color: "#ffffff",
                    fillColor: "#a6d854",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 10
                },
                "large": {
                    color: "#ffffff",
                    fillColor: "#a6d854",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 15
                },
                "mega": {
                    color: "#ffffff",
                    fillColor: "#a6d854",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 20
                }
            },
            "construction-applied": {
                "small": {
                    color: "#ffffff",
                    fillColor: "#66c2a5",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 5
                },
                "medium": {
                    color: "#ffffff",
                    fillColor: "#66c2a5",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 10
                },
                "large": {
                    color: "#ffffff",
                    fillColor: "#66c2a5",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 15
                },
                "mega": {
                    color: "#ffffff",
                    fillColor: "#66c2a5",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 20
                }
            },
            "survey-approved": {
                "small": {
                    color: "#ffffff",
                    fillColor: "#fc8d62",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 5
                },
                "medium": {
                    color: "#ffffff",
                    fillColor: "#fc8d62",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 10
                },
                "large": {
                    color: "#ffffff",
                    fillColor: "#fc8d62",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 15
                },
                "mega": {
                    color: "#ffffff",
                    fillColor: "#fc8d62",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 20
                }
            },
            "survey-applied": {
                "small": {
                    color: "#ffffff",
                    fillColor: "#e78ac3",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 5
                },
                "medium": {
                    color: "#ffffff",
                    fillColor: "#e78ac3",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 10
                },
                "large": {
                    color: "#ffffff",
                    fillColor: "#e78ac3",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 15
                },
                "mega": {
                    color: "#ffffff",
                    fillColor: "#e78ac3",
                    weight: 3,
                    fillOpacity: 1,
                    opacity: 0.8,
                    radius: 20
                }
            }
        },
        "marker-shapes": {
            "small": "TriangleMarker",
            "medium": "CircleMarker",
            "large": "SquareMarker",
            "mega": "HexagonMarker"
        }
    },
    "filter-search-by-elements": [{
        title: "No Filter",
        icon: "img/ui-filter-search-by-icon-none.png",
        className: "ui-filter-search-by-icon"
    }, {
        title: "organization",
        icon: "img/ui-filter-search-by-icon-river.png",
        className: "ui-filter-search-by-icon"
    }, {
        title: "total_beneficiaries",
        icon: "img/ui-filter-search-by-icon-promoter.png",
        className: "ui-filter-search-by-icon"
    },{
        title: "district",
        icon: "img/ui-filter-search-by-icon-none.png",
        className: "ui-filter-search-by-icon"
    }, {
        title: "vdc_municipality",
        icon: "img/ui-filter-search-by-icon-none.png",
        className: "ui-filter-search-by-icon"
    }],
    "special-function-parameters": {
        "operational-year-range": [1970, 2015]
    },

    "popup-docdef": {
            "slider": "pictures"
        ,
        header: ["title"],
        tabs: [{
                title: "Project Details",
                content: {
                    "Total Beneficiaries Direct": "total_beneficiaries_direct",
                    "Total Beneficiaries Indirect": "total_beneficiaries_indirect",
                    "Total Funding": "total_funding",
                    "Sectors": "sectors",
                    "Donor": "donor",
                    "Local Partner": "local_partner",
                    "Organization": "organization"
                }
            },
            {
                title: "Time/Location",
                content: {
                    "Start Date": "date_start",
                    "End Date": "date_end",
                    "Single/Multi Hazard": "single_or_multi_hazard",
                    "VDC/Municipality": "vdc_municipality",
                    "District": "district",
                    "Development Region": "development_region"
                }
            },
            {
                title: "Personnel",
                content: {
                    "DDMPS": "ddmps",
                    "LDRMPS": "ldrmps_completed",
                    "LDRMPS": "ldrmps_planned",
                    "Ward DMPs": "ward_dmps"
                }
            },
            {
                title: "Weblinks",
                content: {
                    "LinkedIn": "linked_in",
                    "Logo": "logo",
                    "Logo-B": "logob"
                }
            }
        ]
    },



    colorList: ["#66c2a5", "#fc8d62", "#8da0cb", "#e78ac3", "#a6d854"],
    opacity: 0.8
};

function setRandomStyles(colorList, opacity) {
    return {
        fillColor: colorList[Math.floor(Math.random() * colorList.length)],
        opacity: opacity
    };
}

toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-left",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
};
