##Map is Live at: http://nrrc-flagship4.kathmandulivinglabs.org
##About this map:
This is map of NRRC Flagship 4 Project locations. Each point represents a project under NRRC Flagship 4, and each numbered circle represents number of projects that are in close proximity in the region. Data for individual projects are updated by their respective organizations; the database is updated several times a day in batch.

##Tech:
1. Clientside: jQuery (using the Cartomancer.js architecture, see architectural documentation for details)
2. Serverside: details at https://gitlab.com/jedi-Knight/ifrc-map-serverside

© 2016 Pratik Gautam. All rights reserved. 
(Author’s email: gautamxpratik@gmail.com)